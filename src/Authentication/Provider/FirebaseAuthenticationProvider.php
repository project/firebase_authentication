<?php

namespace Drupal\firebase_authentication\Authentication\Provider;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\firebase_authentication\FirebaseManager;
use Firebase\Auth\Token\Exception\InvalidToken;
use Kreait\Firebase\Exception\AuthException;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Exception\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Firebase Authentication provider.
 */
class FirebaseAuthenticationProvider implements AuthenticationProviderInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Firebase interface.
   *
   * @var \Drupal\firebase_authentication\FirebaseManager
   */
  protected $firebaseManager;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs the event subscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\firebase_authentication\FirebaseManager $firebase_manager
   *   The Firebase manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FirebaseManager $firebase_manager, LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->firebaseManager = $firebase_manager;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request) {
    $params = (array) json_decode($request->getContent());
    return array_key_exists('id_token', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request) {
    // Creates the firebase authentication client.
    $auth = $this->firebaseManager->authClient();

    // Get the JSON parameters.
    $params = json_decode($request->getContent());

    // Verify token. If valid returns the Drupal user.
    try {
      $verifiedIdToken = $auth->verifyIdToken($params->id_token);
      $firebase_uid = $verifiedIdToken->claims()->get('sub');

      return current($this->entityTypeManager->getStorage('user')->loadByProperties([
        'field_firebase_user_id' => $firebase_uid,
      ]));
    }
    catch (InvalidToken $e) {
      $this->logger->error("[firebase_auth] InvalidToken: " . $e->getMessage());
      return NULL;
    }
    catch (InvalidArgumentException $e) {
      $this->logger->error("[firebase_auth] InvalidArgumentException: " . $e->getMessage());
      return NULL;
    }
    catch (AuthException $e) {
      $this->logger->error("[firebase_auth] AuthException: " . $e->getMessage());
      return NULL;
    }
    catch (FirebaseException $e) {
      $this->logger->error("[firebase_auth] FirebaseException: " . $e->getMessage());
      return NULL;
    }
  }

}
