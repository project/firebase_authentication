<?php

namespace Drupal\firebase_authentication\Plugin\rest\resource;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Session\AccountInterface;
use Drupal\firebase_authentication\FirebaseAuthenticationConstants;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\Plugin\rest\resource\EntityResourceAccessTrait;
use Drupal\rest\Plugin\rest\resource\EntityResourceValidationTrait;
use Drupal\firebase_authentication\FirebaseManager;
use Drupal\user\UserInterface;
use Kreait\Firebase\Exception\AuthException;
use Kreait\Firebase\Exception\FirebaseException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Firebase Drupal User Registration resource.
 *
 * @RestResource(
 *   id = "firebase_authentication_user_registration",
 *   label = @Translation("Firebase Authentication user register"),
 *   serialization_class = "Drupal\user\Entity\User",
 *   uri_paths = {
 *     "create" = "/firebase_authentication/user/register",
 *   }
 * )
 */
class FirebaseAuthenticationUserRegistrationResource extends ResourceBase {

  use EntityResourceValidationTrait;
  use EntityResourceAccessTrait;

  /**
   * User settings config instance.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $userSettings;

  /**
   * Firebase Authentication settings config instance.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $firebaseAuthenticationSettings;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Firebase interface.
   *
   * @var \Drupal\firebase_authentication\FirebaseManager
   */
  protected $firebaseManager;

  /**
   * Constructs a new UserRegistrationResource instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ImmutableConfig $user_settings
   *   A user settings config instance.
   * @param \Drupal\Core\Config\ImmutableConfig $firebase_authentication_settings
   *   The firebase_authentication config instance.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\firebase_authentication\FirebaseManager $firebase_manager
   *   The Firebase manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, ImmutableConfig $user_settings, ImmutableConfig $firebase_authentication_settings, AccountInterface $current_user, FirebaseManager $firebase_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->userSettings = $user_settings;
    $this->firebaseAuthenticationSettings = $firebase_authentication_settings;
    $this->currentUser = $current_user;
    $this->firebaseManager = $firebase_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('config.factory')->get('user.settings'),
      $container->get('config.factory')->get('firebase_authentication.settings'),
      $container->get('current_user'),
      $container->get('plugin.manager.firebase')
    );
  }

  /**
   * Responds to Firebase Authentication user registration POST request.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account entity.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post(UserInterface $account = NULL) {
    try {
      $this->ensureAccountCanRegister($account);
    }
    catch (BadRequestHttpException $exception) {
      throw new BadRequestHttpException($exception->getMessage());
    }

    // Creates the firebase authentication client.
    $auth = $this->firebaseManager->authClient();

    // Create the user in the Firebase with email and password.
    try {
      $signUpResult = $auth->createUserWithEmailAndPassword($account->getEmail(), $account->getPassword());
      // Check if firebase user was successfully created.
      if (!$signUpResult->uid) {
        throw new BadRequestHttpException(t('An error occurred to create firebase user.'));
      }
    }
    catch (AuthException $e) {
      throw new BadRequestHttpException($e->getMessage());
    }
    catch (FirebaseException $e) {
      throw new BadRequestHttpException($e->getMessage());
    }

    // Set Drupal account username.
    $account->setUsername($account->getEmail());
    // Set Drupal account Firebase UID.
    $account->set('field_firebase_user_id', $signUpResult->uid);
    // Set account active.
    $account->activate();
    // Create the account.
    try {
      $account->save();
    }
    catch (EntityStorageException $e) {
      throw new BadRequestHttpException($e->getMessage());
    }

//    $this->sendEmailNotifications($account);

    return new ModifiedResourceResponse(FirebaseAuthenticationConstants::SUCCESS, 200);
  }

  /**
   * Ensure the account can be registered in this request.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account to register.
   */
  protected function ensureAccountCanRegister(UserInterface $account = NULL) {
    if ($account === NULL) {
      throw new BadRequestHttpException(FirebaseAuthenticationConstants::ACCOUNT_INVALID);
    }
    elseif (empty($account->getEmail())) {
      throw new BadRequestHttpException(FirebaseAuthenticationConstants::EMAIL_EMPTY);
    }
    elseif (!\Drupal::service('email.validator')->isValid($account->getEmail())) {
      throw new BadRequestHttpException(FirebaseAuthenticationConstants::EMAIL_INVALID);
    }
    elseif (empty($account->getPassword())) {
      throw new BadRequestHttpException(FirebaseAuthenticationConstants::PASSWORD_EMPTY);
    }
  }

  /**
   * Sends email notifications if necessary for user that was registered.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account.
   */
  protected function sendEmailNotifications(UserInterface $account) {
    $approval_settings = $this->userSettings->get('register');
    // No e-mail verification is required. Activating the user.
    if ($approval_settings == UserInterface::REGISTER_VISITORS) {
      if ($this->userSettings->get('verify_mail')) {
        // No administrator approval required.
        _user_mail_notify('register_no_approval_required', $account);
      }
    }
    // Administrator approval required.
    elseif ($approval_settings == UserInterface::REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL) {
      _user_mail_notify('register_pending_approval', $account);
    }
  }

}
