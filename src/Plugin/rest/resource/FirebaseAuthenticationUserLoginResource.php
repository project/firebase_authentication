<?php

namespace Drupal\firebase_authentication\Plugin\rest\resource;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Session\AccountInterface;
use Drupal\firebase_authentication\FirebaseAuthenticationConstants;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\Plugin\rest\resource\EntityResourceAccessTrait;
use Drupal\rest\Plugin\rest\resource\EntityResourceValidationTrait;
use Drupal\firebase_authentication\FirebaseManager;
use Drupal\user\UserInterface;
use Kreait\Firebase\Exception\FirebaseException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Firebase Authentication User Login resource.
 *
 * @RestResource(
 *   id = "firebase_authentication_user_login",
 *   label = @Translation("Firebase Authentication user login"),
 *   serialization_class = "Drupal\user\Entity\User",
 *   uri_paths = {
 *     "create" = "/firebase_authentication/user/login",
 *   }
 * )
 */
class FirebaseAuthenticationUserLoginResource extends ResourceBase {

  use EntityResourceValidationTrait;
  use EntityResourceAccessTrait;

  /**
   * User settings config instance.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $userSettings;

  /**
   * Firebase Authentication settings config instance.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $firebaseAuthenticationSettings;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Firebase interface.
   *
   * @var \Drupal\firebase_authentication\FirebaseManager
   */
  protected $firebaseManager;

  /**
   * Constructs a new UserLoginResource instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ImmutableConfig $user_settings
   *   The user settings config instance.
   * @param \Drupal\Core\Config\ImmutableConfig $firebase_authentication_settings
   *   The firebase_authentication config instance.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\firebase_authentication\FirebaseManager $firebase_manager
   *   The Firebase manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, ImmutableConfig $user_settings, ImmutableConfig $firebase_authentication_settings, AccountInterface $current_user, FirebaseManager $firebase_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->userSettings = $user_settings;
    $this->firebaseAuthenticationSettings = $firebase_authentication_settings;
    $this->currentUser = $current_user;
    $this->firebaseManager = $firebase_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('config.factory')->get('user.settings'),
      $container->get('config.factory')->get('firebase_authentication.settings'),
      $container->get('current_user'),
      $container->get('plugin.manager.firebase')
    );
  }

  /**
   * Responds to firebase drupal user login POST request.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account entity.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   */
  public function post(UserInterface $account = NULL) {
    try {
      $this->ensureAccountCanLogin($account);
    }
    catch (BadRequestHttpException $exception) {
      throw new BadRequestHttpException($exception->getMessage());
    }

    // Creates the firebase authentication client.
    $auth = $this->firebaseManager->authClient();

    // Sign in the user in the Firebase with email and password.
    try {
      $signInResult = $auth->signInWithEmailAndPassword($account->getEmail(), $account->getPassword());
      // Check if firebase user id exists.
      if (!$signInResult->firebaseUserId()) {
        throw new BadRequestHttpException(FirebaseAuthenticationConstants::FIREBASE_USER_NOT_EXISTS);
      }
    }
    catch (FirebaseException $e) {
      throw new BadRequestHttpException($e->getMessage());
    }

    // Check if drupal exists, if not create the drupal account.
    if (!$user = user_load_by_mail($account->getEmail())) {
      // Set Drupal account username.
      $account->setUsername($account->getEmail());
      // Set Drupal account Firebase UID.
      $account->set('field_firebase_user_id', $signInResult->firebaseUserId());
      // Set account active.
      $account->activate();
      // Create the account.
      try {
        $account->save();
      }
      catch (EntityStorageException $e) {
        throw new BadRequestHttpException($e);
      }
    }

    // Response with firebase token.
    return new ModifiedResourceResponse($signInResult->asTokenResponse(), 200);
  }

  /**
   * Ensure the account can be registered in this request.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account to register.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   */
  protected function ensureAccountCanLogin(UserInterface $account = NULL) {
    if ($account === NULL) {
      throw new BadRequestHttpException(FirebaseAuthenticationConstants::ACCOUNT_INVALID);
    }
    elseif (empty($account->getEmail())) {
      throw new BadRequestHttpException(FirebaseAuthenticationConstants::EMAIL_EMPTY);
    }
    elseif (!\Drupal::service('email.validator')->isValid($account->getEmail())) {
      throw new BadRequestHttpException(FirebaseAuthenticationConstants::EMAIL_INVALID);
    }
    elseif (empty($account->getPassword())) {
      throw new BadRequestHttpException(FirebaseAuthenticationConstants::PASSWORD_EMPTY);
    }
  }

}
