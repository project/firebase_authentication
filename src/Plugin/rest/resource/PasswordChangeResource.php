<?php

namespace Drupal\firebase_authentication\Plugin\rest\resource;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Session\AccountInterface;
use Drupal\firebase_authentication\FirebaseAuthenticationConstants;
use Drupal\firebase_authentication\FirebaseManager;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\Plugin\rest\resource\EntityResourceAccessTrait;
use Drupal\rest\Plugin\rest\resource\EntityResourceValidationTrait;
use Drupal\user\Entity\User;
use Kreait\Firebase\Exception\FirebaseException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Firebase Authentication User Password Change resource.
 *
 * @RestResource(
 *   id = "firebase_authentication_password_change",
 *   label = @Translation("Firebase Authentication user password change"),
 *   uri_paths = {
 *     "create" = "/firebase_authentication/user/password/change",
 *   }
 * )
 */
class PasswordChangeResource extends ResourceBase {

  use EntityResourceValidationTrait;
  use EntityResourceAccessTrait;

  /**
   * User settings config instance.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $userSettings;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Firebase interface.
   *
   * @var \Drupal\firebase_authentication\FirebaseManager
   */
  protected $firebaseManager;

  /**
   * Constructs a new PasswordChangeResource instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ImmutableConfig $user_settings
   *   The user settings config instance.
   * @param \Drupal\Core\Config\ImmutableConfig $firebase_authentication_settings
   *   The firebase_authentication config instance.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\firebase_authentication\FirebaseManager $firebase_manager
   *   The Firebase manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, ImmutableConfig $user_settings, ImmutableConfig $firebase_authentication_settings, AccountInterface $current_user, FirebaseManager $firebase_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->userSettings = $user_settings;
    $this->firebaseAuthenticationSettings = $firebase_authentication_settings;
    $this->currentUser = $current_user;
    $this->firebaseManager = $firebase_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('config.factory')->get('user.settings'),
      $container->get('config.factory')->get('firebase_authentication.settings'),
      $container->get('current_user'),
      $container->get('plugin.manager.firebase')
    );
  }

  /**
   * Responds to firebase drupal user password change POST request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request data.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The HTTP response object.
   */
  public function post(Request $request) {
    $user = User::load($this->currentUser->id());
    if (empty($user)) {
      return new JsonResponse([
        'message' => FirebaseAuthenticationConstants::DRUPAL_USER_NOT_EXISTS,
      ], 400);
    }

    // Validate if user have a Firebase ID.
    if (!$firebase_id = $user->get('field_firebase_user_id')->value) {
      return new JsonResponse([
        'message' => FirebaseAuthenticationConstants::USER_NOT_HAVE_FIREBASE_ID
      ], 400);
    }

    // Get the JSON Request parameters.
    $params = json_decode($request->getContent());

    if (empty($params->new_password)) {
      return new JsonResponse([
        'message' => FirebaseAuthenticationConstants::PASSWORD_EMPTY,
      ], 400);
    }

    // Creates the firebase authenticator client.
    $auth = $this->firebaseManager->authClient();

    // Change user password.
    try {
      $pwd_change_result = $auth->changeUserPassword($firebase_id, $params->new_password);
    }
    catch (FirebaseException $e) {
      $this->logger->error($e->getMessage());
      return new JsonResponse([
        'message' => FirebaseAuthenticationConstants::PASSWORD_CHANGE_ERROR,
      ], 400);
    }

    if (!$pwd_change_result->uid) {
      return new JsonResponse([
        'message' => FirebaseAuthenticationConstants::PASSWORD_CHANGE_ERROR,
      ], 400);
    }

    // Success response. Password changed.
    return new JsonResponse([
      'message' => FirebaseAuthenticationConstants::SUCCESS,
    ], 200);
  }

}
