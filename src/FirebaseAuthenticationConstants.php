<?php

namespace Drupal\firebase_authentication;

/**
 * Firebase Authentication Constants class.
 */
class FirebaseAuthenticationConstants {

  /**
   * Success code.
   */
  const SUCCESS = 'success';

  /**
   * Generic Error code.
   */
  const ERROR = 'error';

  /**
   * Drupal account is invalid.
   */
  const ACCOUNT_INVALID = 'account_invalid';

  /**
   * Email empty.
   */
  const EMAIL_EMPTY = 'email_empty';

  /**
   * Email is invalid.
   */
  const EMAIL_INVALID = 'email_invalid';

  /**
   * Password change error.
   */
  const PASSWORD_CHANGE_ERROR = 'password_change_error';

  /**
   * Drupal user not exists.
   */
  const DRUPAL_USER_NOT_EXISTS = 'drupal_user_not_exists';

  /**
   * Firebase user not exists.
   */
  const FIREBASE_USER_NOT_EXISTS = 'firebase_user_not_exists';

  /**
   * User not have firebase id.
   */
  const USER_NOT_HAVE_FIREBASE_ID = 'user_not_have_firebase_id';

  /**
   * Password empty
   */
  const PASSWORD_EMPTY = 'password_empty';
}
