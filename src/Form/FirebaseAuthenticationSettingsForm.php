<?php

namespace Drupal\firebase_authentication\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Class FirebaseAuthSettingsForm.
 *
 * Firebase Auth Settings admin form.
 *
 * @package Drupal\firebase_authentication\Form
 */
class FirebaseAuthenticationSettingsForm extends ConfigFormBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'firebase_authentication.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'firebase_authentication_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['firebase_authentication_settings'] = [
      '#type' => 'container',
      '#prefix' => '<div id="firebase-authentication-settings">',
      '#suffix' => '</div>',
      '#weight' => 10,
    ];

    $form['firebase_authentication_settings']['private_key_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private key path'),
      '#default_value' => $this->config('firebase_authentication.settings')->get('private_key_path'),
      '#description' => $this->t('
Please enter your Firebase Private key file path. You can generate this file in @link.<br />
<strong>Is strongly recommended to put this file in a folder outside your repository (for security reasons).</strong>
',
        [
          '@link' => Link::fromTextAndUrl(
            'Firebase Service Accounts',
            Url::fromUri('https://console.firebase.google.com/project/_/settings/serviceaccounts/adminsdk'))->toString(),
        ]),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('firebase_authentication.settings')
      ->set('private_key_path', (string) $form_state->getValue('private_key_path'))
      ->save(TRUE);

    parent::submitForm($form, $form_state);
  }

}
